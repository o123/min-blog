<?php
return [
    'DEBUG' => [
        'level' => 0,
        'type' => 'auto',
        'log' => 2,
    ],
    'ROUTER' => [
        'mod' => 2,
        'module' => 0,
        'restfull' => null,
    ],
];
