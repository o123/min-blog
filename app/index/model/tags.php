<?php
namespace model;

use root\base\model;
use root\state\tags as cache;
use root\state\art;

class tags extends model
{
    const TABLE = 'tag';
    private function data()
    {
        $data = [];
        $tag = F('tag');
        $sort = ID('sort', -1);
        $show = ID('show', -1);
        $mark = F('mark');
        if (isset($tag)) {
            if (!$tag) {
                return $this->error(['msg' => '请填写标签名称', 'field' => 'tag']);
            } elseif (32 < mb_strlen($tag)) {
                return $this->error(['msg' => '标签名称不能超过32个字符', 'field' => 'tag']);
            } else {
                $data['tag'] = $tag;
            }
        }
        if (isset($mark)) {
            if (255 < mb_strlen($mark)) {
                return $this->error(['msg' => '备注信息不能超过255个字符', 'field' => 'mark']);
            } else {
                $data['mark'] = $mark;
            }
        }
        -1 < $show && $data['show'] = $show;
        -1 < $sort && $data['sort'] = $sort;
        if (!$data) {
            return $this->error('非法操作');
        }
        return $data;
    }
    public function get()
    {
        $db = $this->db(self::TABLE);
        $data = $db->order('sort')->select();
        return $data;
    }
    public function add()
    {
        if (!$data = $this->data()) {
            return false;
        }
        if (!isset($data['tag'])) {
            return $this->error(['msg' => '请填写标签名称', 'field' => 'tag']);
        }
        $db = $this->db(self::TABLE);
        if ($data['tid'] = (int) $db->Insert($data)) {
            cache::del();
            return $data;
        } else {
            return $this->error($db->getError() ?: '未知错误');
        }
    }
    public function set($tid)
    {
        if (!$data = $this->data()) {
            return false;
        }
        $db = $this->db(self::TABLE);
        if ($db->where("`tid`={$tid}")->Update($data)) {
            cache::del();
            return $data;
        } else {
            return $this->error($db->getError() ?: '没有变更');
        }
    }
    public function del($tid)
    {
        $where = ['tid' => $tid];
        $db = $this->db(self::TABLE);
        $db->Begin();
        if ($n = $db->where($where)->Delete()) {
            $ids = $db->table('art_tag')->where($where)->Select('id');
            $db->table('art_tag')->where($where)->Delete();
            if ($ids) {
                if ($sets = $db->table('art_tag')->Field('`id`, GROUP_CONCAT(`tid`) as `tids`')->Where(['id'=>$ids])->Group('`id`')->Select()) {
                    set_time_limit(0);
                    $db->table('article');
                    foreach($sets as $v) {
                        $isset[] = $v['id'];
                        $db->Where("`id`={$v['id']}")->Update(['tids'=>$v['tids']]);
                        art::setCache($v['id'], false);
                    }
                    $null = array_diff($ids, $isset);
                } else {
                    $null = $ids;
                }
                if ($null) {
                    $db->table('article');
                    $db->Where(['id'=>$null])->Update(['tids'=>'']);
                    foreach($null as $id) {
                        art::setCache($id, false);
                    }
                }
                if ($navs = SYS['NAV'] ?? null) {
                    $is = false;
                    foreach ($navs as $k => $v) {
                        if ($tid === $v['tid']) {
                            unset($navs[$k]);
                            $is = true;
                            break;
                        }
                    }
                    if ($is) {
                        $sets = SYS;
                        $sets['NAV'] = $navs;
                        $str = "<?php\nreturn " . var_export($sets, true) . ';';
                        file_put_contents(P_ROOT . 'config/system.config.php', $str);
                    }
                }
            }
            $db->Commit();
            cache::del();
            return $n;
        } else {
            return $this->error($db->getError() ?: '删除失败');
        }
    }
}
